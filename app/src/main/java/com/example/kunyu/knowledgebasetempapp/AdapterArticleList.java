package com.example.kunyu.knowledgebasetempapp;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kunyu.knowledgebasetempapp.databinding.ChildArticleGridListBinding;

import java.util.List;

public class AdapterArticleList extends RecyclerView.Adapter<AdapterArticleList.ViewHolder>{
    private AdapterArticleListListener mAdapterArticleListListener;
    private List<EducationItem> mEducationArticleList;
    // 123456
    AdapterArticleList(@NonNull List<EducationItem> educationArticles) {
        applyDataList(educationArticles);
    }

    // 223
    public void applyDataList(List<EducationItem> educationArticles){
        mEducationArticleList = educationArticles;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ChildArticleGridListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.child_article_grid_list, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if(recyclerView.getContext() instanceof AdapterArticleListListener)
            mAdapterArticleListListener = (AdapterArticleListListener) recyclerView.getContext();
        else
            throw new RuntimeException(recyclerView.getContext().toString() + " must implement " + AdapterArticleListListener.class.getName());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EducationItem educationArticle = mEducationArticleList.get(position);
        holder.bind(educationArticle);
    }

    @Override
    public int getItemCount() {
        return mEducationArticleList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ChildArticleGridListBinding mBinding;

        ViewHolder(ChildArticleGridListBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(EducationItem educationArticle){
            mBinding.setDataItem(educationArticle);
            mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EducationItem educationItem = mBinding.getDataItem();
                    mAdapterArticleListListener.onArticleClicked(
                            educationItem.contentUrl,
                            educationItem.getName()
                    );
                }
            });
        }
    }

    interface AdapterArticleListListener{
        void onArticleClicked(String url, String title);
    }
}
