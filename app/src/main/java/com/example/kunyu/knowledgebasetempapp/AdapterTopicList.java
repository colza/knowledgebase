package com.example.kunyu.knowledgebasetempapp;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kunyu.knowledgebasetempapp.databinding.ChildTopicListBinding;

import java.util.List;


public class AdapterTopicList extends RecyclerView.Adapter<AdapterTopicList.ViewHolder>{
    private List<Topic> mTopicList;
    private AdapterTopicListListener mAdapterTopicListListener;

    public AdapterTopicList(List<Topic> topicList) {
        applyDataList(topicList);
    }

    public void applyDataList(List<Topic> topicList){
        mTopicList = topicList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ChildTopicListBinding childTopicListBinding = DataBindingUtil.inflate(layoutInflater, R.layout.child_topic_list, parent, false);
        return new ViewHolder(childTopicListBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mTopicList.get(position));
    }

    @Override
    public int getItemCount() {
        return mTopicList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if(recyclerView.getContext() instanceof AdapterTopicListListener)
            mAdapterTopicListListener = (AdapterTopicListListener) recyclerView.getContext();
        else
            throw new RuntimeException(recyclerView.getContext().toString() + " must implement " + AdapterTopicListListener.class.getName());
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ChildTopicListBinding mBinding;

        ViewHolder(ChildTopicListBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(Topic topic){
            mBinding.setTopic(topic);
            mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAdapterTopicListListener.onTopicClicked(mBinding.getTopic().getTopicId());
                }
            });
        }
    }

    interface AdapterTopicListListener{
        void onTopicClicked(int topicId);
    }
}
