package com.example.kunyu.knowledgebasetempapp;


import android.os.Parcel;

public class EducationArticle extends EducationItem {

    long _id;

    int weekIndex;

    public EducationArticle() {}

    public EducationArticle(String contentUrl) {
        super(contentUrl);
    }

    public long get_id() {
        return _id;
    }

    public int getWeekIndex() {
        return weekIndex;
    }

    public boolean isDueNow(int currentWeekIndex) {
        return ! isConsumed() &&
                getWeekIndex() <= currentWeekIndex;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeLong(this._id);
        dest.writeInt(this.weekIndex);
    }

    protected EducationArticle(Parcel in) {
        super(in);
        this._id = in.readLong();
        this.weekIndex = in.readInt();
    }

    public static final Creator<EducationArticle> CREATOR = new Creator<EducationArticle>() {
        public EducationArticle createFromParcel(Parcel source) {
            return new EducationArticle(source);
        }

        public EducationArticle[] newArray(int size) {
            return new EducationArticle[size];
        }
    };
}