package com.example.kunyu.knowledgebasetempapp;

import android.os.Parcel;
import android.os.Parcelable;


public class EducationItem implements Parcelable {
    public static final String TYPE_ARTICLE = "article";
    public static final String TYPE_CBT = "cbt";

    String name;

    String brief;

    boolean consumed;

    String contentUrl;

    String imageUrl;

    String type;

    int pageCount;

    public EducationItem(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getName() {
        return name;
    }

    public String getBrief() {
        return brief;
    }

    public boolean isConsumed() {
        return consumed;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public String getImageUrl() {
        if(imageUrl == null)
            return "";
        return imageUrl;
    }

    public String getType() {
        return type;
    }

    public int getPageCount() {
        return pageCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.brief);
        dest.writeByte(consumed ? (byte) 1 : (byte) 0);
        dest.writeString(this.contentUrl);
        dest.writeString(this.imageUrl);
        dest.writeString(this.type);
        dest.writeInt(this.pageCount);
    }

    public EducationItem() {
    }

    protected EducationItem(Parcel in) {
        this.name = in.readString();
        this.brief = in.readString();
        this.consumed = in.readByte() != 0;
        this.contentUrl = in.readString();
        this.imageUrl = in.readString();
        this.type = in.readString();
        this.pageCount = in.readInt();
    }

    public EducationItem(String name, String brief, boolean consumed, String contentUrl, String imageUrl, String type, int pageCount) {
        this.name = name;
        this.brief = brief;
        this.consumed = consumed;
        this.contentUrl = contentUrl;
        this.imageUrl = imageUrl;
        this.type = type;
        this.pageCount = pageCount;
    }

    public static final Creator<EducationItem> CREATOR = new Creator<EducationItem>() {
        public EducationItem createFromParcel(Parcel source) {
            return new EducationItem(source);
        }

        public EducationItem[] newArray(int size) {
            return new EducationItem[size];
        }
    };
}
