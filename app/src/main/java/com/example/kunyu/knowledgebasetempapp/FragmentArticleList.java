package com.example.kunyu.knowledgebasetempapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class FragmentArticleList extends Fragment {
    private RecyclerView mRecyclerView;
    public FragmentArticleList() {
        // Required empty public constructor
    }

    public static FragmentArticleList newInstance() {
        FragmentArticleList fragment = new FragmentArticleList();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_article_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.article_list);
        mRecyclerView.setAdapter(new AdapterArticleList(getFakeList()));
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mRecyclerView.addItemDecoration(new SpacesItemDecoration(20));
        return view;
    }

    private List<EducationItem> getFakeList(){
        List<EducationItem> list = new ArrayList<>();
        list.add(new EducationItem(
                "name",
                "brief",
                false,
                "http://www.google.com.uk",
                null,
                "type",
                10
        ));
        list.add(new EducationItem(
                "name",
                "brief",
                false,
                "http://www.google.com.uk",
                null,
                "type",
                10
        ));
        list.add(new EducationItem(
                "name",
                "brief",
                false,
                "http://www.google.com.uk",
                null,
                "type",
                10
        ));
        return list;
    }

    @Override
    public void onResume() {
        super.onResume();
    }


}
