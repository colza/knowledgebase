package com.example.kunyu.knowledgebasetempapp;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.annotations.NonNull;

public class GetTopicsUseCase extends SingleUseCase<Integer, List<Topic>>{

    public GetTopicsUseCase() {
    }

    @Override
    public Single<List<Topic>> buildUseCaseObservable(Integer integer) {
        return Single.create(new SingleOnSubscribe<List<Topic>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<List<Topic>> e) throws Exception {
                e.onSuccess(getFakeTopicList());
            }
        });
    }

    private List<Topic> getFakeTopicList(){
        List<Topic> list = new ArrayList<>();
        list.add(new Topic("title", 5, 10));
        list.add(new Topic("title", 5, 10));
        list.add(new Topic("title", 5, 10));
        list.add(new Topic("title", 5, 10));
        return list;
    }
}
