package com.example.kunyu.knowledgebasetempapp;

import android.content.IntentFilter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.annotations.NonNull;

public class GetUnreadArticlesUseCase extends SingleUseCase<Integer, List<EducationItem>>{

    public GetUnreadArticlesUseCase() {
    }

    @Override
    public Single<List<EducationItem>> buildUseCaseObservable(Integer param) {
        return Single.create(new SingleOnSubscribe<List<EducationItem>>() {
            @Override
            public void subscribe(@NonNull SingleEmitter<List<EducationItem>> e) throws Exception {
                e.onSuccess(getFakeList());
            }
        });
    }

    private List<EducationItem> getFakeList(){
        List<EducationItem> list = new ArrayList<>();
        list.add(new EducationItem(
                "name",
                "brief",
                false,
                "http://www.google.com.uk",
                null,
                "type",
                10
        ));
        list.add(new EducationItem(
                "name",
                "brief",
                false,
                "http://www.google.com.uk",
                null,
                "type",
                10
        ));
        return list;
    }
}
