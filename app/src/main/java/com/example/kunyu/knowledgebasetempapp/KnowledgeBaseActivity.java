package com.example.kunyu.knowledgebasetempapp;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class KnowledgeBaseActivity extends AppCompatActivity implements
        SliderChild.OnClickedSliderChildListener,
        AdapterArticleList.AdapterArticleListListener,
        AdapterTopicList.AdapterTopicListListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knowledgebase);

        if (savedInstanceState == null) {
            addFragmentWithTagToActivity(getSupportFragmentManager(), KnowledgeBaseMain.newInstance(), R.id.activity_container, KnowledgeBaseMain.class.getSimpleName());
        }
    }

    public void addFragmentWithTagToActivity(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment, final int frameId, @NonNull final String tag) {
        if (!fragment.isAdded()) {
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(frameId, fragment, tag);
            transaction.commit();
        }
    }

    public void addFragmentWithTagToActivity(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment, final int frameId, final String tag,
                                             final String backStackName) {
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment, tag);
        transaction.addToBackStack(backStackName);
        transaction.commit();
    }

    @Override
    public void onSliderViewClicked(String url, String title) {
        Log.i("LOG","Clicked on slider " + url + " title = " + title);
    }

    @Override
    public void onArticleClicked(String url, String title) {
        Log.i("LOG","Clicked on article " + url + " title = " + title);
    }

    @Override
    public void onTopicClicked(int topicId) {
        Log.i("LOG","Clicked on topic " + topicId);
        addFragmentWithTagToActivity(
                getSupportFragmentManager(),
                FragmentArticleList.newInstance(),
                R.id.activity_container,
                FragmentArticleList.class.getSimpleName(),
                FragmentArticleList.class.getSimpleName());
    }
}
