package com.example.kunyu.knowledgebasetempapp;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kunyu.knowledgebasetempapp.databinding.FragmentKnowledgeBaseMainBinding;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableSingleObserver;


public class KnowledgeBaseMain extends Fragment {
    private FragmentKnowledgeBaseMainBinding mViewBinding;
    GetTopicsUseCase mGetTopicsUseCase;
    GetUnreadArticlesUseCase mGetUnreadArticlesUseCase;
    KnowledgeBaseSliderBannerAdapter2 knowledgeBaseSliderBannerAdapter2;

    public KnowledgeBaseMain() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static KnowledgeBaseMain newInstance() {
        KnowledgeBaseMain fragment = new KnowledgeBaseMain();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mViewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_knowledge_base_main, container, false);
        knowledgeBaseSliderBannerAdapter2 = new KnowledgeBaseSliderBannerAdapter2(getFragmentManager(), getFakeList());
        mViewBinding.viewPager.setAdapter(knowledgeBaseSliderBannerAdapter2);
        mViewBinding.circleIndicator.setViewPager(mViewBinding.viewPager);
        knowledgeBaseSliderBannerAdapter2.registerDataSetObserver(mViewBinding.circleIndicator.getDataSetObserver());

        mViewBinding.unreadList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mViewBinding.unreadList.setNestedScrollingEnabled(false);
        mViewBinding.unreadList.addItemDecoration(new SpacesItemDecoration(20));

        mViewBinding.topicList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mViewBinding.topicList.setNestedScrollingEnabled(false);
        mViewBinding.topicList.addItemDecoration(new SpacesItemDecoration(20));

        mViewBinding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                knowledgeBaseSliderBannerAdapter2 = new KnowledgeBaseSliderBannerAdapter2(getFragmentManager(), getFakeLis2());

                knowledgeBaseSliderBannerAdapter2.applyDataList(getFakeLis2());
//                knowledgeBaseSliderBannerAdapter2.clearData();
//                mViewBinding.viewPager.setAdapter(knowledgeBaseSliderBannerAdapter2);
//                mViewBinding.circleIndicator.setViewPager(mViewBinding.viewPager);
//                knowledgeBaseSliderBannerAdapter2.registerDataSetObserver(mViewBinding.circleIndicator.getDataSetObserver());
            }
        });

        return mViewBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        // apply UseCase to fire the requests.

        mGetTopicsUseCase = new GetTopicsUseCase();
        mGetTopicsUseCase.execute(new TopicObserver(), 1);

//        mGetUnreadArticlesUseCase = new GetUnreadArticlesUseCase();
//        mGetUnreadArticlesUseCase.execute(new UnreadArticlesObserver(), 1);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGetTopicsUseCase != null) mGetTopicsUseCase.dispose();
    }

    private class UnreadArticlesObserver extends DisposableSingleObserver<List<EducationItem>> {

        public UnreadArticlesObserver() {
        }

        @Override
        public void onSuccess(@NonNull List<EducationItem> list) {
            mViewBinding.setIsUnreadListEmpty(list.isEmpty());
            mViewBinding.unreadList.setAdapter(new AdapterArticleList(list));

            mViewBinding.setIsSliderListEmpty(list.isEmpty());
            knowledgeBaseSliderBannerAdapter2.applyDataList(list);
        }

        @Override
        public void onError(@NonNull Throwable e) {
            mViewBinding.setIsUnreadListEmpty(true);
        }
    }

    private class TopicObserver extends DisposableSingleObserver<List<Topic>> {
        private TopicObserver() {
        }

        @Override
        public void onSuccess(@NonNull List<Topic> topicList) {
            mViewBinding.topicList.setAdapter(new AdapterTopicList(topicList));
        }

        @Override
        public void onError(@NonNull Throwable e) {

        }
    }

    private List<EducationItem> getFakeList() {
        List<EducationItem> list = new ArrayList<>();
        list.add(new EducationItem(
                "name1",
                "brief1",
                false,
                "http://www.google.com.uk",
                null,
                "type",
                10
        ));
        list.add(new EducationItem(
                "name2",
                "brief2",
                false,
                "http://www.google.com.uk",
                null,
                "type",
                10
        ));
        return list;
    }

    private List<EducationItem> getFakeLis2() {
        List<EducationItem> list = new ArrayList<>();
        list.add(new EducationItem(
                "name3",
                "brief",
                false,
                "http://www.google.com.uk",
                null,
                "type",
                10
        ));

        return list;
    }


}
