package com.example.kunyu.knowledgebasetempapp;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class KnowledgeBaseSliderBannerAdapter2 extends FragmentStatePagerAdapter {
    private List<EducationItem> mEducationItemList = new ArrayList<>();
    public KnowledgeBaseSliderBannerAdapter2(FragmentManager fm, @NonNull List<EducationItem> list) {
        super(fm);
        applyDataList(list);
    }

    public void applyDataList(@NonNull List<EducationItem> list){
//        mEducationItemList.clear();
//        mEducationItemList.addAll(list);
        mEducationItemList = list;
        notifyDataSetChanged();
    }


    public void clearData(){
        mEducationItemList.clear();
        notifyDataSetChanged();
    }
    @Override
    public Fragment getItem(int position) {
        return SliderChild.newInstance(mEducationItemList.get(position));
    }


    @Override
    public int getCount() {
        return mEducationItemList.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
