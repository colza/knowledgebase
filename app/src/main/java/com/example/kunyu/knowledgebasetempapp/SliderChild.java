package com.example.kunyu.knowledgebasetempapp;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kunyu.knowledgebasetempapp.databinding.FragmentSliderChildBinding;

public class SliderChild extends Fragment {
    static final String EDUCATION_ITEM = "EDUCATION_ITEM";
    private OnClickedSliderChildListener mListener;

    public SliderChild() {
        // Required empty public constructor
    }

    public static SliderChild newInstance(EducationItem educationArticle) {
        SliderChild fragment = new SliderChild();
        Bundle args = new Bundle();
        args.putParcelable(EDUCATION_ITEM, educationArticle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentSliderChildBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_slider_child, container, false);
        final EducationItem educationItem = getArguments().getParcelable(EDUCATION_ITEM);
        binding.image.setImageResource(R.drawable.kitten);
        binding.subtitle.setText(educationItem.getName());
        binding.title.setText(educationItem.getBrief());
        binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSliderViewClicked(educationItem.contentUrl, educationItem.getName());
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClickedSliderChildListener) {
            mListener = (OnClickedSliderChildListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement " + SliderChild.class.getName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    interface OnClickedSliderChildListener {
        void onSliderViewClicked(String url, String title);
    }
}
