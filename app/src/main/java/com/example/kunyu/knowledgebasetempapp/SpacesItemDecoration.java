package com.example.kunyu.knowledgebasetempapp;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        outRect.bottom = space;
        int layoutPosition = parent.getChildLayoutPosition(view);
        int halfSpace = space/2;
        if(layoutPosition%2 == 0)
            outRect.right = halfSpace;
        else
            outRect.left = halfSpace;
    }
}
