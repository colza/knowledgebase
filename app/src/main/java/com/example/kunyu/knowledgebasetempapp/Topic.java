package com.example.kunyu.knowledgebasetempapp;

public class Topic{
    private String title;
    private int topicId;
    private int unreadCount;

    public Topic(String title, int topicId, int unreadCount) {
        this.title = title;
        this.topicId = topicId;
        this.unreadCount = unreadCount;
    }

    public String getTitle() {
        return title;
    }

    public int getTopicId() {
        return topicId;
    }

    public String getUnreadCount() {
        return String.valueOf(unreadCount);
    }
}
