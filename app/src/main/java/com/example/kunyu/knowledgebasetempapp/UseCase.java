package com.example.kunyu.knowledgebasetempapp;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public abstract class UseCase<Params, Response> {
    // code change
    private final CompositeDisposable disposables;

    UseCase() {
        this.disposables = new CompositeDisposable();
    }

    /**
     * Builds an {@link Observable} which will be used when executing the current {@link UseCase}.
     */
    public abstract Observable<Response> buildUseCaseObservable(Params params);

    /**
     * Executes the current use case.
     *
     * @param observer {@link DisposableObserver} which will be listening to the observable build
     *                 by {@link #buildUseCaseObservable(Params)} ()} method.
     * @param params   Parameters (Optional) used to build/execute this use case.
     */
    public void execute(DisposableObserver<Response> observer, Params params) {
        if (observer != null) {
            final Observable<Response> observable = this.buildUseCaseObservable(params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
            addDisposable(observable.subscribeWith(observer));
        }
    }

    /**
     * Dispose from current {@link CompositeDisposable}.
     */
    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    /**
     * Dispose from current {@link CompositeDisposable}.
     */
    private void addDisposable(Disposable disposable) {
        // Supposedly disposables would never be null, just in case.
        if (disposable != null && disposables != null)
            disposables.add(disposable);
    }

    /**
     * Call this method to your UseCase to cancel the RxJava request. It has similar behaviour when
     * we cancel AsyncTask. Here, the Observable would still generate the event and invoke onNext(),
     * however the Observer would never receive the event. It's like cancelling AsyncTask, the
     * doInBackground still process but the onPostExecute wouldn't trigger.
     *
     * All the UseCase should be disposed at least at onDestroy() to make sure there's no memory leak.
     * The leakage is cause by the background thread is still running even the activity is destroyed.
     *
     * And you should dispose it at onPause() if the request is fired at onResume(). If you don't do
     * so, there would be many request being sent if we keep resuming the application.
     *
     * @param useCase
     */
    public static void dispose(UseCase useCase) {
        if (useCase != null) useCase.dispose();
    }

    /**
     * Same as {@link #dispose(UseCase)}
     *
     * @param disposable
     */
    public static void dispose(Disposable disposable) {
        if (disposable != null && !disposable.isDisposed()) disposable.dispose();
    }
}

